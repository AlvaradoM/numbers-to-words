const units = ['','one','two','three','four','five','six','seven','eight','nine']
const teens = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
const tens = ['', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

function numbersToWords(){

    for (let count=1; count<=1000; count++){
        if (count < 10){
            document.write(count + " " +units[count] + "<br>")
        }
        else if (count > 9 && count < 20){
            document.write(count + " " + teens[count - 10] + "<br>")
        }
        else if (count >= 20 && count <=99){
            let bothDigits = String(count);
            let firstDigit = Number(bothDigits[0])
            let secondDigit = Number(bothDigits[1])
            let underHundred = tens[firstDigit - 1] + " " + units[secondDigit]
            document.write(count + " " + underHundred + "<br>")
        }        
        else if (count >= 100 && count <= 999) {

            let allDigits = String(count);
            let firstDigit = Number(allDigits[0])
            let secondDigit = Number(allDigits[1])
            let thirdDigit = Number(allDigits[2])
            if (secondDigit == 0){
                let hundreds = units[firstDigit] + " hundred " + units[thirdDigit]
                document.write(count + " " + hundreds + "<br")
            }
            else if (secondDigit == 1){
                let hundreds = units[firstDigit] + " hundred " + teens[thirdDigit]
                document.write(count + " " + hundreds + "<br>")
            }
            else{
                let underHundred = tens[secondDigit - 1] + " " + units[thirdDigit]
                let hundreds = units[firstDigit] + " hundred " + underHundred

                document.write(count + " " + hundreds + "<br>")
            }
        }
        else{
            document.write(count + " one thousand" )
        }
 
        

    }


}
numbersToWords()
